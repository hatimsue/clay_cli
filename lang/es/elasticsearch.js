let index = {
    title:"Elasticsearch",
    items:[
        {
            title:"Descripción",
            text:"descripcion de ejemplo",
            items:[
                {
                    comand:"lynx https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html",
                    description:"Documentación"
                }
            ]
        },
        {
            title:"Instalación",
            text: "descripcion de instalacion y link oficial",
            items:[
                {
                    comand:"wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -",
                    description:"Download and install the public signing key"
                },
                {
                    comand:"sudo apt-get install apt-transport-https",
                    description:"You may need to install the apt-transport-https package on Debian before proceeding"
                    
                },  
                ,
                {
                    comand:'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list',
                    description:"Save the repository definition to /etc/apt/sources.list.d/elastic-7.x.list"
                },
                {
                    comand:"sudo apt-get update && sudo apt-get install elasticsearch",
                    description:"You can install the Elasticsearch Debian package with"
                    
                },
                {
                    comand:"sudo systemctl start elasticsearch.service",
                    description:"Running Elasticsearch with systemdedit"
                    
                }, 
                {
                    comand:"curl -XGET http://127.0.0.1:9200",
                    description:"Checking that Elasticsearch is runningedit"
                    
                }
                
            ]
        },
        {
            title:"Uso",
            text:"A continuación se describen los comandos principales:",
            items:[
                {
                    comand:"comando1",
                    description:"este comando..."
                },
                {
                    comand:"comando 2",
                    description:"con este comando ..."
                }//mas itemsc
            ]
        }
    ]
}
module.exports = index;