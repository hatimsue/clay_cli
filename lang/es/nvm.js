let index = {
    title:"NVM",
    items:[
        {
            title:"Descripción",
            text:"NVM es una herramienta que nos permite instalar multiples versiones de nodejs en nuestro sistema operativo, intercambiarlas entre sí y desinstalarlas según se requiera.",
            items:[
                {
                    comand:"lynx https://github.com/nvm-sh/nvm",
                    description:"Documentación"
                }
            ]
        },
        {
            title:"Instalación",
            text: "Para installarlo vamos a ir a la pagina de github y vamos a seguir las instrucciones ( https://github.com/nvm-sh/nvm )",
            items:[
                {
                    comand:"curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash",
                    description:"curl: podemos instalarlo con el siguiente comando"
                },
                {
                    comand:"wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash",
                    description:"wget: o podemos installarlo mediante wget con este comando"
                },
                ,
                {
                    comand:"nvm --version",
                    description:"verificación: para verificar que NVM está instalado en el sistema vamos a ejecutar este comando que nos mostrará la version de nvm que instalamos"
                }
                
            ]
        },
        {
            title:"Uso",
            text:"A continuación se describen los comandos principales:",
            items:[
                {
                    comand:"nvm ls-remote",
                    description:"este comando nos mostrará todas las versiones de node disponibles para instalar"
                },
                {
                    comand:"nvm install <version>",
                    description:"con este comando instalamos una version específica de node"
                },
                {
                    comand:"nvm install node",
                    description:"con este comando instalamos la última version disponible de  node"
                },
                {
                    comand:"nvm ls",
                    description:"listar todas las versiones instaladas de node"
                },
                {
                    comand:"nvm use node",
                    description:"usar la última versión disponible de node"
                },
                {
                    comand:"nvm use <version>",
                    description:"usar la version de especificada de node"
                }
            ]
        }
    ]
}
module.exports = index;