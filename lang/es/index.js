let nvm = require('./nvm.js')
let logstash = require('./logstash.js')
let kibana = require('./kibana.js')
let elasticsearch = require('./elasticsearch.js')
let filebeat = require('./filebeat.js')

let index = {
    title:"Tabla de contenido",
    message:"Selecciona una opción",
    description:"Con la ayuda de este script podremos ver los comandos principales de nvm, express, docker... y tambien podremos ver algunos ejemplos de uso",
    items:[
        nvm,
        elasticsearch,
        kibana,
        logstash,
        filebeat
    ],
    back:"Atrás",
    quit:"Salir"
}

module.exports = index;