/**
 * packages
 */
let strings = require("./lang/es/index.js")
let editor = require("./main/editor.js")
let inquirer = require('inquirer')

/**
 * main object
 */

let  tuto = {
    options:[...strings.items.map((a,i)=>{return `${i+1}: ${a.title}`}),`${strings.items.length+1}: ${strings.quit}`],
    methods:{
        init:(callback)=>{
            process.stdout.write('\033c')
            inquirer
            .prompt([
            {
                type: 'list',
                name: 'index',
                message: strings.message,
                choices: [new inquirer.Separator(),...tuto.options]
            }
            ])
            .then (answers => {
                callback(answers.index.split(': ')[1])
            })
        },
        goNext:(str)=>{
            process.stdout.write('\033c')
            if(str==strings.quit)
                return 0
            let current = strings.items.find(a=>{return a.title==str})
            let currentItems = current.items.map((a,i)=>{return `${i+1}: ${a.title}`})
            inquirer
            .prompt([
            {
                type: 'list',
                name: 'index',
                message: strings.message,
                choices: [new inquirer.Separator(),...currentItems,`${currentItems.length+1}: ${strings.back}`]
            }
            ])
            .then (answers => {
                if(answers.index==`${currentItems.length+1}: ${strings.back}`){
                    tuto.methods.init(tuto.methods.goNext)
                }else{
                    tuto.methods.printItem(answers.index.split(': ')[1],current,str)
                }
                
            })
        },
        printItem:(str, currentItems,str1)=>{
            let current = currentItems.items.find(a=>{return a.title==str})
            process.stdout.write('\033c')
            editor.printItem(current,2)
            inquirer
            .prompt([
            {
                type: 'list',
                name: 'index',
                message: strings.message,
                choices: [new inquirer.Separator(),strings.back]
            }
            ])
            .then (answers => {
                tuto.methods.goNext(str1) 
            })
            
        }
    }
}


tuto.methods.init(tuto.methods.goNext)







