let clc = require("cli-color")
let cliFormat = require('cli-format')
let editor = {}


editor.title= (title)=>{
    console.log(clc.blue.underline(title))
    console.log()
}
editor.h1= (title,sangria)=>{
    let result = cliFormat.wrap(title, {justify: true,paddingLeft:sangria});
    console.log(clc.cyan(result))
    console.log()
}
editor.h2= (text,sangria)=>{
    let result = cliFormat.wrap(text, { justify: true,paddingLeft:sangria});
    console.log(clc.blue(result))
    console.log()
}
editor.comand=(comand)=>{
    console.log(clc.yellow(comand))
    console.log()
}

editor.printItem=(item, level)=>{
    let sangria = editor.getSangria(level)
        if(item.title){
            editor.title(sangria+item.title)
        }
        if(item.text){
            editor.h1(item.text,sangria)
        }
        if(item.description){
            editor.h2(item.description,sangria)
        }
        if(item.comand){
            editor.comand(sangria+'$ '+item.comand)
        }
        if(item.items){
            item.items.map(a=>{
                editor.printItem(a,level+1)
            })
        }  
}
editor.getSangria=(level)=>{
    let start = ""
    let concat = "   "
    for(let i = 0; i<level; i++){
        start+=concat
    }
    return start

}


module.exports = editor